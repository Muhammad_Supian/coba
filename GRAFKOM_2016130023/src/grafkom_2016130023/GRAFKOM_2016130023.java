/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafkom_2016130023;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 *
 * @author Muhammad Supian
 */
public class GRAFKOM_2016130023 {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

    private static void createAndShowGUI() {
        JFrame f = new JFrame("Swing Paint Demo");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(1000, 1000);
        f.setLocationRelativeTo(null);
        f.add(new LatihanUI());
        f.setVisible(true);
    }
}
