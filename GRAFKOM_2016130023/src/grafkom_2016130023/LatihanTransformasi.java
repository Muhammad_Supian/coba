/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafkom_2016130023;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import javax.swing.JPanel;

/**
 *
 * @author Muhammad Supian
 */
public class LatihanTransformasi extends JPanel {

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        g2.translate(300, 200);
        Ellipse2D.Float ling1 = new Ellipse2D.Float(0, 0, 200, 200);
        Rectangle r = new Rectangle(55, 55, 90, 90);
        //g2.draw(ling1);
        g2.rotate(Math.PI / 4.0);
        //g2.draw(r);

        Area kancing = new Area(ling1);
        kancing.subtract(new Area(r));
        g2.setColor(Color.blue);
        g2.fill(kancing);

    }
}
