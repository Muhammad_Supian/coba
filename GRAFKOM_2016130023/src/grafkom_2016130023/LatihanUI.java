/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafkom_2016130023;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;

/**
 *
 * @author Muhammad Supian
 */
public class LatihanUI extends JPanel implements MouseListener {

    private Area rumah;
    private int warna;
    private Area w1;
    private Area w2;
    private Area w3;
    private Area w4;
    private Area w5;

    public LatihanUI() {
        addMouseListener(this);

        Rectangle2D.Float a = new Rectangle2D.Float(100, 100, 75, 150);
        Rectangle2D.Float C = new Rectangle2D.Float(125, 125, 10, 15);

        GeneralPath my = new GeneralPath();
        my.moveTo(75, 100);
        my.lineTo(200, 100);
        my.lineTo(138, 50);
        my.closePath();

        AffineTransform tr = new AffineTransform();

        this.rumah = new Area(a);
        this.rumah.add(new Area(my));
        this.rumah.exclusiveOr(new Area(C));

        Ellipse2D.Float ww1 = new Ellipse2D.Float(110, 350, 50, 50);
        this.w1 = new Area(ww1);
        Ellipse2D.Float ww2 = new Ellipse2D.Float(185, 350, 50, 50);
        this.w2 = new Area(ww2);
        Ellipse2D.Float ww3 = new Ellipse2D.Float(260, 350, 50, 50);
        this.w3 = new Area(ww3);
        Ellipse2D.Float ww4 = new Ellipse2D.Float(335, 350, 50, 50);
        this.w4 = new Area(ww4);
        Ellipse2D.Float ww5 = new Ellipse2D.Float(410, 350, 50, 50);
        this.w5 = new Area(ww5);

    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        switch (this.warna) {
            case 1:
                g2.setColor(Color.RED);
                break;
            case 2:
                g2.setColor(Color.BLUE);
                break;
            case 3:
                g2.setColor(Color.YELLOW);
                break;
            case 4:
                g2.setColor(Color.GREEN);
                break;
            case 5:
                g2.setColor(Color.CYAN);
        }

        g2.fill(this.rumah);

        g2.setColor(Color.red);
        g2.fill(w1);

        g2.setColor(Color.BLUE);
        g2.fill(w2);

        g2.setColor(Color.YELLOW);
        g2.fill(w3);

        g2.setColor(Color.GREEN);
        g2.fill(w4);

        g2.setColor(Color.CYAN);
        g2.fill(w5);
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (this.w1.contains(e.getX(), e.getY())) {
            if (e.getButton() == MouseEvent.BUTTON1) {
                this.warna = 1;
            }
        } else if (this.rumah.contains(e.getX(), e.getY())) {
            if (e.getButton() == MouseEvent.BUTTON1) {
                this.warna = 2;
            }
        } else if (this.rumah.contains(e.getX(), e.getY())) {
            if (e.getButton() == MouseEvent.BUTTON1) {
                this.warna = 3;
            }
        } else if (this.rumah.contains(e.getX(), e.getY())) {
            if (e.getButton() == MouseEvent.BUTTON1) {
                this.warna = 4;
            }
        } else if (this.w5.contains(e.getX(), e.getY())) {
            if (e.getButton() == MouseEvent.BUTTON1) {
                this.warna = 5;
            }
        }

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        //  throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseExited(MouseEvent e) {
        //   throw new UnsupportedOperationException("Not supported yet.");
    }

}
