/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafkom_2016130023;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import javax.swing.JPanel;

/**
 *
 * @author Muhammad Supian
 */
public class UjiCobaTransformasi extends JPanel {

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        g2.translate(200, 200);
        Rectangle r = new Rectangle(0, 0, 300, 200);
        g2.draw(r);
//    
//    g2.setColor(Color.red);
//    g2.rotate(Math.PI/4.0);
//    g2.draw(r);
//    
//    g2.setColor(Color.blue);
//    g2.scale(2, 2);
//    g2.draw(r);

//    g2.setColor(Color.blue);
//    g2.shear(0, 1);
//    g2.draw(r);
        GeneralPath myShape = new GeneralPath();
        myShape.moveTo(200, 200);
        myShape.curveTo(300, 100, 500, 300, 600, 200);
        myShape.lineTo(600, 400);
        myShape.curveTo(500, 500, 300, 300, 200, 400);
        myShape.closePath();
        g2.draw(myShape);

        AffineTransform old = g2.getTransform(); //save

        g2.translate(400, 300);  //tengah bendera
        g2.scale(1.25, 1.4);
        g2.translate(-400, -300);   //kembalikan posisi
        g2.setColor(Color.blue);
        g2.draw(myShape);

        g2.setTransform(old);   //load
        g2.draw(myShape);   //original

//    Ellipse2D.Float ling1 = new Ellipse2D.Float(100,100,100,100);
//    Ellipse2D.Float ling2 = new Ellipse2D.Float(150,100,100,100);
//    g2.draw(ling1);
//    g2.draw(ling2);
//    
//    Area daun = new Area(ling1);
//    daun.intersect(new Area(ling2));
//    g2.fill(daun);

//    Ellipse2D.Float ling1 = new Ellipse2D.Float(100,100,200,200);
//    Ellipse2D.Float ling2 = new Ellipse2D.Float(150,150,100,100);
//    g2.draw(ling1);
//    g2.draw(ling2);
//    
//    Area donat = new Area(ling1);
//    donat.subtract(new Area(ling2));
//    g2.fill(donat);
        Ellipse2D.Float ling1 = new Ellipse2D.Float(100, 100, 200, 200);
        Ellipse2D.Float ling2 = new Ellipse2D.Float(150, 150, 100, 100);
        g2.draw(ling1);
        g2.draw(ling2);

        AffineTransform tr = new AffineTransform();
        tr.scale(1.25, 1.7);

        Area donat = new Area(ling1);
        donat.subtract(new Area(ling2));
        donat.transform(tr);
        g2.fill(donat);
    }
}
